CREATE DATABASE exam10;
use exam10;

CREATE TABLE news
(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(25) NOT NULL,
    content VARCHAR(200) NOT NULL,
    date_of_delivery datetime,
    image varchar(50),
    PRIMARY KEY (id)
);

CREATE TABLE comments
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    news_id INT NOT NULL,
    author VARCHAR(25) DEFAULT 'Anonymous',
    content VARCHAR(200) NOT NULL,
    FOREIGN KEY (news_id) REFERENCES news(id) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO news(title, content, date_of_delivery,image) VALUES ('vsvds', 'vsdvdsvdvdsvd', '2020-02-18', 'ez_MVQdZtzizxKwe3HxH4.jpg'),('gergreger', 'reger', '2020-02-20', 'NI9EhjEmmY34P5NHYcDRF.jpg' );
INSERT INTO comments(news_id, content) VALUES (1, 'grgregergreger'),(2, 'rrrrrrr' ),(1,'nfngfngfngf');

