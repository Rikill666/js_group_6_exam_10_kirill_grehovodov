const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require("../config");
const mysqlDB = require("../mysqlDB");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const router = express.Router();
const upload = multer({storage});

//все элементы
router.get('/', async (req, res) => {
    const news = await mysqlDB.getConnection().query("SELECT * FROM news");
    res.send(news.map(news => {
        return {
            id: news.id,
            title: news.title,
            image: news.image,
            date_of_delivery: news.date_of_delivery
        }
    }));
});

//создание нового
router.post('/', upload.single('image'), async (req, res) => {
    let file = "";
    if (req.file) {
        file = req.file.filename;
    }
    const news = req.body;
    const date = new Date().toLocaleString();
    if (news.title && news.content) {
        try {
            const newNews = await mysqlDB.getConnection().query(`INSERT INTO news (title, content, date_of_delivery, image) VALUES(?, ?, ?, ?)`, [news.title, news.content, date, file]);
            res.send({...news, id: newNews.insertId, date_of_delivery: date, image: file});
        } catch (e) {
            console.log(e);
            return res.status(400).send({message: "Error"});
        }
    } else {
        res.status(400).send({error: "Required fields are not entered"});
    }
});

//элемент по id
router.get("/:id", async (req, res) => {
    const id = req.params.id;
    const data = await mysqlDB.getConnection().query("SELECT * FROM news WHERE id=?", [id]);
    if (!data[0]) {
        return res.status(404).send({message: "Not found"});
    }
    res.send(data[0]);
});

//удаление
router.delete("/:id", async (req, res) => {
    const id = req.params.id;
    await mysqlDB.getConnection().query("DELETE FROM news WHERE id=?", [id]);
    res.send("ok");
});

//изменение
router.put("/:id", upload.single('image'), async (req, res) => {
    const id = req.params.id;
    const news = await mysqlDB.getConnection().query("SELECT * FROM news WHERE id=?", [id]);

    let file = "";
    if (news[0].image) {
        file = news[0].image;
    }
    if (req.file) {
        file = req.file.filename;
    }
    try {
        const date = new Date().toLocaleString();
        await mysqlDB.getConnection().query("UPDATE news SET content = ?, title = ?, date_of_delivery = ?, image = ? WHERE id = ?", [req.body.content, req.body.title, date, file, id]);
        const updateNews = await mysqlDB.getConnection().query("SELECT * FROM news WHERE id=?", [id]);
        res.send(updateNews);
    } catch (e) {
        console.log(e);
        return res.status(400).send({message: "Error"});
    }
});


module.exports = router;