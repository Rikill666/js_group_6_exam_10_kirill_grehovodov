const express = require('express');
const mysqlDB = require("../mysqlDB");

const router = express.Router();

//все элементы или по id новости
router.get('/', async (req, res) => {
    let comments = [];
    if(req.query.news_id){
        comments = await mysqlDB.getConnection().query("SELECT * FROM comments WHERE news_id=?", [req.query.news_id]);
    }
    else{
        comments = await mysqlDB.getConnection().query("SELECT * FROM comments");
    }
    if (comments.length < 1) {
        return res.send([]);
    }
    res.send(comments.map(comment => {
        return {
            id:comment.id,
            content: comment.content,
            author: comment.author
        }
    }));
});

//создание нового
router.post('/', async (req, res) => {
    const comment = req.body;
    if (comment.news_id && comment.content) {
        const news = await mysqlDB.getConnection().query("SELECT * FROM news WHERE id=?", [comment.news_id]);

        if (!news[0]) {
            return res.status(404).send({message: "Not found"});
        } else {
            let author = comment.author?comment.author:"Anonymous";
            const newComment = await mysqlDB.getConnection().query(`INSERT INTO comments (content, news_id, author) VALUES (?, ?, ?)`, [comment.content, comment.news_id, author]);
            res.send({...comment, id: newComment.insertId});
        }
    } else {
        res.status(400).send({error: "No news selected or comment written"});
    }
});


//удаление
router.delete("/:id", async (req, res) => {
    const id = req.params.id;
    await mysqlDB.getConnection().query("DELETE FROM comments WHERE id=?", [id]);
    res.send("ok");
});

//изменение
router.put("/:id", async (req, res) => {
    const id = req.params.id;
    const comment = req.body;
    try {
        await mysqlDB.getConnection().query("UPDATE comments SET content = ?, news_id = ?, author=? WHERE id=?", [comment.content, comment.news_id,comment.author, id]);
        const data = await mysqlDB.getConnection().query("SELECT * FROM comments WHERE id=?", [id]);
        res.send(data);
    } catch (e) {
        console.log(e);
        return res.status(400).send({message: "Non-existing news selected"});
    }
});


module.exports = router;