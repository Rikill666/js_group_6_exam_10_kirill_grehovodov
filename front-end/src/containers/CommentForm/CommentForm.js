import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {addComment} from "../../store/actions/commentsActions";

class CommentForm extends Component {

    state = {
        author: '',
        content: '',
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.addComment({...this.state, news_id:this.props.news_id}, this.props.news_id);
        this.setState({author:"", content:""});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Fragment>
                <h4>Add comment</h4>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="author">Name</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="author" id="author"
                                placeholder="Enter name"
                                value={this.state.author}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="content">Comment</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea" required
                                name="content" id="content"
                                placeholder="Enter comment"
                                value={this.state.content}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">Add</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addComment: (comment, news_id) => dispatch(addComment(comment, news_id)),
});

export default connect(null, mapDispatchToProps)(CommentForm);