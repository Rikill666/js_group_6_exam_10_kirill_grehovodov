import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {addNews} from "../../store/actions/newsActions";
import {connect} from "react-redux";

class NewsForm extends Component {

    state = {
        title: '',
        content: '',
        image: ''
    };

    submitFormHandler = async(event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        await this.props.addNews(formData);
        this.props.history.push("/");
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
                <h1>New Post</h1>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="title">Title</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                name="title" id="title"
                                placeholder="Enter title"
                                value={this.state.title}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="content">Content</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea" required
                                name="content" id="content"
                                placeholder="Enter text"
                                value={this.state.content}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="image">Image</Label>
                        <Col sm={10}>
                            <Input
                                type="file"
                                name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">Send</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addNews: (news) => dispatch(addNews(news)),
});

export default connect(null, mapDispatchToProps)(NewsForm);