import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, Col, Row} from "reactstrap";
import {NavLink} from "react-router-dom";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";
import Spinner from "../../components/UI/Spinner/Spinner";
import {deleteNews, getNews} from "../../store/actions/newsActions";
import Moment from "react-moment";

class News extends Component {
    componentDidMount() {
        this.props.getNews();
    }

    render() {
        return (
            <Fragment>
                <div style={{textAlign: "right"}}>
                    <Button style={{margin: "20px"}} tag={NavLink} to={'/news/new'} exact>Add post</Button>
                </div>
                {this.props.loading ? <Spinner/> : this.props.news.map(news => {
                    return <Card style={{marginBottom: "10px"}} key={news.id}>
                        <Row>
                            <Col sm={6} md={2} lg={2}>
                                <NewsThumbnail image={news.image}/>
                            </Col>

                            <Col sm={6} md={10} lg={10} style={{
                                display: "flex",
                                justifyContent: "space-between",
                                flexDirection: "column",
                                margin:"20px 0"
                            }}>
                                    <p>
                                        <strong style={{display: "block"}}>
                                            {news.title}
                                        </strong>
                                    </p>
                                    <div>
                                        <Row>
                                            <Col>
                                                <Moment format="YYYY-MM-DD HH:mm">
                                                    {news.date_of_delivery}
                                                </Moment>
                                            </Col>
                                            <Col>
                                                <Button tag={NavLink} to={'/news/' + news.id} exact>Read fool
                                                    post>></Button>
                                            </Col>
                                            <Col>
                                                <Button
                                                    onClick={() => this.props.deleteNews(news.id)}>Delete</Button>
                                            </Col>
                                        </Row>
                                    </div>
                            </Col>
                        </Row>
                    </Card>
                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news.news,
    loading: state.news.loading,
    error: state.news.error
});

const mapDispatchToProps = dispatch => ({
    getNews: () => dispatch(getNews()),
    deleteNews: (id) => dispatch(deleteNews(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
