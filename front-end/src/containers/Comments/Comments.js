import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import {deleteComment, getComments} from "../../store/actions/commentsActions";

class Comments extends Component {
    componentDidMount() {
        this.props.getComments(this.props.news_id);
    }

    render() {
        return (
            this.props.loading ? <Spinner/> :
                this.props.comments.length > 0 ?
                    this.props.comments.map(comment =>
                        <Fragment key={comment.id}>
                            <h4>Comments</h4>
                            <Card style={{marginBottom: "10px"}}
                            >
                                <CardBody
                                    style={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                                    <p>
                                        <strong>
                                            {comment.author}
                                        </strong> wrote: {comment.content}
                                    </p>
                                    <Button onClick={() => this.props.deleteComment(comment.id, this.props.news_id)}
                                            className="w-25">Delete</Button>
                                </CardBody>
                            </Card>
                        </Fragment>
                    ) : <h2>No comments</h2>
        );
    }
}

const mapStateToProps = state => {
    return {
        comments: state.comment.comments,
        loading: state.comment.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getComments: (news_id) => dispatch(getComments(news_id)),
        deleteComment: (id, news_id) => dispatch(deleteComment(id, news_id)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Comments);