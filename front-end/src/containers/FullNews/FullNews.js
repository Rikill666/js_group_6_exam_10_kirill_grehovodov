import React, {Component, Fragment} from 'react';
import {Card, CardHeader, CardText, CardTitle} from "reactstrap";
import Moment from "react-moment";
import {connect} from "react-redux";
import {getNewsById} from "../../store/actions/newsActions";
import Spinner from "../../components/UI/Spinner/Spinner";
import Comments from "../Comments/Comments";
import CommentForm from "../CommentForm/CommentForm";

class FullNews extends Component {

    componentDidMount() {
        this.props.getNewsById(this.props.match.params.id);
    }
    render() {
        return (
            <Fragment>
                {!this.props.fullNews?<Spinner/>:
                <Card body inverse color="info">
                    <CardHeader>
                        <CardTitle>{this.props.fullNews.title}</CardTitle>
                        <Moment format="YYYY-MM-DD HH:mm">
                            {this.props.fullNews.date_of_delivery}
                        </Moment>
                    </CardHeader>
                    <CardText>{this.props.fullNews.content}</CardText>
                </Card>
                }
                <div style={{marginTop:"25px"}}>
                    <Comments news_id={this.props.match.params.id}/>
                </div>
                    <CommentForm news_id={this.props.match.params.id}/>
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        fullNews: state.news.fullNews,
        loading: state.news.loading,
        error:state.news.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getNewsById: (id) => dispatch(getNewsById(id)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(FullNews);
