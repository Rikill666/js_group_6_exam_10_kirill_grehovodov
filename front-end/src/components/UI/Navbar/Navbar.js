import React from 'react';
import {NavbarBrand} from "reactstrap";
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <NavbarBrand tag={NavLink} to={'/'}>News</NavbarBrand>
    );
};

export default Navbar;