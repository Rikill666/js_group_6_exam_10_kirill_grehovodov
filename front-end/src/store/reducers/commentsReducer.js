import {FETCH_COMMENTS_ERROR, FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS} from "../actions/commentsActions";

const initialState = {
    comments: [],
    loading:false,
    error: null,
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, loading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments, error:null, loading: false};
        case FETCH_COMMENTS_ERROR:
            return {...state,error:action.error, loading: false};

        default:
            return state;
    }
};

export default newsReducer;