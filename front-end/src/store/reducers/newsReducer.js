import {
  FETCH_NEWS_BY_ID_ERROR,
  FETCH_NEWS_BY_ID_REQUEST, FETCH_NEWS_BY_ID_SUCCESS,
  FETCH_NEWS_ERROR,
  FETCH_NEWS_REQUEST,
  FETCH_NEWS_SUCCESS,
} from "../actions/newsActions";

const initialState = {
  news: [],
  fullNews: null,
  loading: false,
  error: null,
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS_REQUEST:
      return {...state, loading: true};
    case FETCH_NEWS_SUCCESS:
      return {...state, news: action.news, error:null, loading: false};
    case FETCH_NEWS_ERROR:
      return {...state,error:action.error, loading: false};
    case FETCH_NEWS_BY_ID_REQUEST:
      return {...state, loading: true};
    case FETCH_NEWS_BY_ID_SUCCESS:
      return {...state, fullNews: action.news, error:null, loading: false};
    case FETCH_NEWS_BY_ID_ERROR:
      return {...state,error:action.error, loading: false};
    default:
      return state;
  }
};

export default newsReducer;