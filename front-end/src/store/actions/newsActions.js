import axiosApi from "../../axiosApi";

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_ERROR = 'FETCH_NEWS_ERROR';

export const FETCH_NEWS_BY_ID_REQUEST = 'FETCH_NEWS_BY_ID_REQUEST';
export const FETCH_NEWS_BY_ID_SUCCESS = 'FETCH_NEWS_BY_ID_SUCCESS';
export const FETCH_NEWS_BY_ID_ERROR = 'FETCH_NEWS_BY_ID_ERROR';

export const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const CREATE_NEWS_ERROR= 'CREATE_NEWS_ERROR';

export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_ERROR= 'DELETE_NEWS_ERROR';

export const deleteNewsRequest = () => {return {type: DELETE_NEWS_REQUEST};};
export const deleteNewsSuccess = () => ({type: DELETE_NEWS_SUCCESS});
export const deleteNewsError = (error) => {return {type: DELETE_NEWS_ERROR, error};};

export const fetchNewsRequest = () => {return {type: FETCH_NEWS_REQUEST};};
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsError = (error) => {return {type: FETCH_NEWS_ERROR, error};};

export const fetchNewsByIdRequest = () => {return {type: FETCH_NEWS_BY_ID_REQUEST};};
export const fetchNewsByIdSuccess = news => ({type: FETCH_NEWS_BY_ID_SUCCESS, news});
export const fetchNewsByIdError = (error) => {return {type: FETCH_NEWS_BY_ID_ERROR, error};};

export const createNewsRequest = () => {return {type: CREATE_NEWS_REQUEST};};
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const createNewsError = (error) => {return {type: CREATE_NEWS_ERROR, error};};

export const getNews = () => {
  return async dispatch => {
    try{
      dispatch(fetchNewsRequest());
      const response = await axiosApi.get('/news');
      const news = response.data;
        dispatch(fetchNewsSuccess(news));
    }
    catch (e) {
      dispatch(fetchNewsError(e));
    }
  };
};

export const getNewsById = (id) => {
  return async dispatch => {
    try{
      dispatch(fetchNewsByIdRequest());
      const response = await axiosApi.get('/news/' + id);
      const news = response.data;
      dispatch(fetchNewsByIdSuccess(news));
    }
    catch (e) {
      dispatch(fetchNewsByIdError(e));
    }
  };
};

export const addNews = (news) => {
  return async dispatch => {
    try{
      dispatch(createNewsRequest());
      await axiosApi.post('/news', news);
      dispatch(createNewsSuccess());
    }
    catch (e) {
      dispatch(createNewsError(e));
    }
  };
};

export const deleteNews = (id) => {
  return async dispatch => {
    try{
      dispatch(deleteNewsRequest());
      await axiosApi.delete('/news/' + id);
      dispatch(deleteNewsSuccess());
      dispatch(getNews());
    }
    catch (e) {
      dispatch(deleteNewsError(e));
    }
  };
};