import axiosApi from "../../axiosApi";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_ERROR = 'FETCH_COMMENTS_ERROR';

export const CREATE_COMMENTS_REQUEST = 'CREATE_COMMENTS_REQUEST';
export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMENTS_SUCCESS';
export const CREATE_COMMENTS_ERROR = 'CREATE_COMMENTS_ERROR';

export const DELETE_COMMENTS_REQUEST = 'DELETE_COMMENTS_REQUEST';
export const DELETE_COMMENTS_SUCCESS = 'DELETE_COMMENTS_SUCCESS';
export const DELETE_COMMENTS_ERROR = 'DELETE_COMMENTS_ERROR';

export const fetchCommentsRequest = () => {
    return {type: FETCH_COMMENTS_REQUEST};
};
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchCommentsError = (error) => {
    return {type: FETCH_COMMENTS_ERROR, error};
};

export const createCommentsRequest = () => {
    return {type: CREATE_COMMENTS_REQUEST};
};
export const createCommentsSuccess = () => ({type: CREATE_COMMENTS_SUCCESS});
export const createCommentsError = (error) => {
    return {type: CREATE_COMMENTS_ERROR, error};
};

export const deleteCommentsRequest = () => {
    return {type: DELETE_COMMENTS_REQUEST};
};
export const deleteCommentsSuccess = () => ({type: DELETE_COMMENTS_SUCCESS});
export const deleteCommentsError = (error) => {
    return {type: DELETE_COMMENTS_ERROR, error};
};


export const getComments = (news_id) => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());
            let response;
            if (news_id) {
                response = await axiosApi.get('/comments?news_id='+news_id);
            } else {
                response = await axiosApi.get('/comments');
            }
            const comments = response.data;

            dispatch(fetchCommentsSuccess(comments));
        } catch (e) {
            dispatch(fetchCommentsError(e));
        }
    };
};

export const addComment = (comment, news_id) => {
    return async dispatch => {
        try {
            dispatch(createCommentsRequest());
            await axiosApi.post('/comments', comment);
            dispatch(createCommentsSuccess());
            dispatch(getComments(news_id));
        } catch (e) {
            dispatch(createCommentsError(e));
        }
    };
};

export const deleteComment = (id, news_id) => {
    return async dispatch => {
        try {
            dispatch(deleteCommentsRequest());
            await axiosApi.delete('/comments/' + id);
            dispatch(deleteCommentsSuccess());
            dispatch(getComments(news_id));
        } catch (e) {
            dispatch(deleteCommentsError(e));
        }
    };
};