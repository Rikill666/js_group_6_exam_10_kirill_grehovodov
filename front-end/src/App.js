import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import News from "./containers/News/News";
import NewsForm from "./containers/NewsForm/NewsForm";
import Navbar from "./components/UI/Navbar/Navbar";
import FullNews from "./containers/FullNews/FullNews";

const App = () => {
    return (
        <Container>
            <Navbar/>
            <Switch>
                <Route path="/" exact component={News}/>
                <Route path="/news" exact component={News}/>
                <Route path="/news/new" exact component={NewsForm}/>
                <Route path="/news/:id" exact component={FullNews}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
        </Container>
    );
};

export default App;